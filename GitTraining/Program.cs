﻿using System;
using System.Linq;

namespace GitTraining
{
    class Program
    {
        static void Main(string[] args)
        {
            string line;
            while ((line = (Console.ReadLine() ?? "").Trim()) != "")
            {
                var s = line.Split(' ').Select(sub => sub.Trim()).ToArray();
                var arg1 = decimal.Parse(s[0]);
                var operation = s[1];
                var arg2 = decimal.Parse(s[2]);

                decimal result = 0;
                switch (operation)
                {
                    case "+":
                        result = Sum(arg1, arg2);
                        break;
                    case "-":
                        result = Whoever(arg1,arg2);
                        break;
                }
                Console.WriteLine("> " + result);
            }
        }

        private static decimal Minus(decimal a, decimal b)
        {
            return a - b;
        }

        private static decimal Sum(decimal a, decimal b)
        {
            return a + b;
        }
    }
}
